import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/hello',
      name: 'hello',
      component: () => import('../views/HelloView.vue')
    },
    {
      path: '/plus',
      name: 'plus',
      component: () => import('../views/PlusView.vue')
    },
    {
      path: '/color',
      name: 'color',
      component: () => import('../views/ColorView.vue')
    },
    {
      path: '/counter',
      name: 'counter',
      component: () => import('../views/CounterView.vue')
    },
    {
      path: '/grade',
      name: 'grade',
      component: () => import('../views/GradeView.vue')
    },
    {
      path: '/light',
      name: 'light',
      component: () => import('../views/LightView.vue')
    },
    {
      path: '/bmi',
      name: 'bmi',
      component: () => import('../views/BmiView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/todo',
      name: 'todo',
      component: () => import('../views/TodoView.vue')
    },
    {
      path: '/ctof',
      name: 'ctof',
      component: () => import('../views/TemperatureView.vue')
    },
    {
      path: '/user',
      name: 'user',
      component: () => import('../views/UserView.vue')
    },
    {
      path: '/image',
      name: 'image',
      component: () => import('../views/UploadImageView.vue')
    },
    {
      path: '/chart',
      name: 'chart',
      component: () => import('../views/ChartView.vue')
    }
  ]
})

export default router
