import { ref, computed, reactive } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('User', () => {
  const user = reactive({
    email: '',
    password: ''
  })
  const result = ref('')
  const isLogin = ref(false)
  function login() {
    if (user.email === 'user1@mail.com' && user.password === 'Pass@1234') {
      result.value = 'Login Success!!!'
      isLogin.value = true
      return
    }
    isLogin.value = false
    result.value = 'Login Fail!!!'
  }

  return { user, result, isLogin, login }
})
